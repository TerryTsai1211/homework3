﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Homewrok3
{
    public partial class iButton : ContentView
    {
        public event EventHandler iBtnClick;

        public string iBtnText
        {
            get { return btn.Text; }
            set { btn.Text = value; }
        }

        public Color iBtnTextColor
        {
            get { return btn.TextColor; }
            set { btn.TextColor = value; }
        }

        public iButton()
        {
            InitializeComponent();

            btn.Clicked += (sender, e) => 
            {
                // 新寫法
                iBtnClick ?.DynamicInvoke(sender , e);

                // 舊寫法
                //if (iBtnClick != null)
                //{
                //    iBtnClick(sender, e);
                //}

                // Click 觸發後，把文字變成粗體和斜體
                btn.FontAttributes = FontAttributes.Bold | FontAttributes.Italic;
            };
        }
    }
}
