﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using Xamarin.Forms.Xaml;

namespace Homewrok3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            ibtn.iBtnClick += Ibtn_iBtnClick;

            // 在 C# 上指定不同平台設定值
            //ibtn.iBtnColor = Device.OnPlatform<Color>(
            //    Color.Green,
            //    Color.Red,
            //    Color.Blue);

            //ibtn.iBtnText = Device.OnPlatform<string>(
            //    iBtnTextFromStatic.Text4iOS,
            //    iBtnTextFromStatic.Text4Android,
            //    iBtnTextFromStatic.Text4WindowsPhone);
        }

        private void Ibtn_iBtnClick(object sender, EventArgs e)
        {
            ((Button)sender).Text = "使用者點選 Button";
        }
    }
}
